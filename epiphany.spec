%global glib2_version 2.61.2
%global gtk3_version 3.24.0
%global webkit2gtk3_version 2.27.3
Name:                epiphany
Epoch:               1
Version:             3.38.5
Release:             2
Summary:             Web browser for GNOME
License:             GPL-3.0+ and LGPL-2.1 and MIT and GPL+ and ISC
URL:                 https://wiki.gnome.org/Apps/Web
Source0:             https://download.gnome.org/sources/epiphany/3.38/%{name}-%{version}.tar.xz
Patch0:              epiphany-default-bookmarks-openeuler.patch
Patch1:              epiphany-dont-make-compulsory.patch

BuildRequires:       desktop-file-utils gcc gettext-devel iso-codes-devel itstool
BuildRequires:       libappstream-glib-devel meson pkgconfig(cairo) pkgconfig(evince-document-3.0)
BuildRequires:       pkgconfig(gcr-3) pkgconfig(gdk-3.0) >= %{gtk3_version}
BuildRequires:       pkgconfig(gdk-pixbuf-2.0) pkgconfig(gio-unix-2.0) >= %{glib2_version}
BuildRequires:       pkgconfig(glib-2.0) >= %{glib2_version}
BuildRequires:       pkgconfig(gnome-desktop-3.0) >= %{glib2_version}
BuildRequires:       pkgconfig(gtk+-3.0) >= %{gtk3_version}
BuildRequires:       pkgconfig(gtk+-unix-print-3.0) >= %{gtk3_version} pkgconfig(hogweed)
BuildRequires:       pkgconfig(icu-uc) pkgconfig(json-glib-1.0) pkgconfig(libdazzle-1.0)
BuildRequires:       pkgconfig(libhandy-1) pkgconfig(libnotify) pkgconfig(libportal) pkgconfig(libsecret-1)
BuildRequires:       pkgconfig(libsoup-2.4) pkgconfig(libxml-2.0) pkgconfig(libxslt)
BuildRequires:       pkgconfig(nettle) pkgconfig(sqlite3)
BuildRequires:       pkgconfig(webkit2gtk-4.0) >= %{webkit2gtk3_version}
BuildRequires:       pkgconfig(webkit2gtk-web-extension-4.0) >= %{webkit2gtk3_version} chrpath
Requires:            %{name}-runtime%{?_isa} = %{epoch}:%{version}-%{release}
%description
Epiphany is the web browser for the GNOME desktop. Its goal is to be
simple and easy to use. Epiphany ties together many GNOME components
in order to let you focus on the Web content, instead of the browser
application.

%package runtime
Summary:             Epiphany runtime suitable for web applications
Requires:            gsettings-desktop-schemas gtk3%{?_isa} >= %{gtk3_version} iso-codes
Requires:            webkit2gtk3%{?_isa} >= %{webkit2gtk3_version}
%description runtime
This package provides a runtime for web applications without actually
installing the epiphany application itself.

%prep
%autosetup -p1

%build
%meson
%meson_build

%install
%meson_install
%find_lang %{name} --with-gnome

chrpath -d %{buildroot}%{_libdir}/epiphany/*.so
chrpath -d %{buildroot}%{_libdir}/epiphany/web-process-extensions/*.so
chrpath -d %{buildroot}%{_libexecdir}/epiphany/*
chrpath -d %{buildroot}%{_libexecdir}/epiphany-search-provider
chrpath -d %{buildroot}%{_bindir}/epiphany

mkdir -p %{buildroot}%{_sysconfdir}/ld.so.conf.d
echo "%{_libdir}/epiphany" > %{buildroot}%{_sysconfdir}/ld.so.conf.d/%{name}-%{_arch}.conf

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%check
desktop-file-validate $RPM_BUILD_ROOT%{_datadir}/applications/*.desktop

%files -f %{name}.lang
%{_libexecdir}/epiphany-search-provider
%{_datadir}/applications/org.gnome.Epiphany.desktop
%{_datadir}/dbus-1/services/org.gnome.Epiphany.SearchProvider.service
%{_datadir}/metainfo/org.gnome.Epiphany.appdata.xml
%dir %{_datadir}/gnome-shell/
%dir %{_datadir}/gnome-shell/search-providers/
%{_datadir}/gnome-shell/search-providers/org.gnome.Epiphany.SearchProvider.ini
%config(noreplace) %{_sysconfdir}/ld.so.conf.d/%{name}-%{_arch}.conf

%files runtime
%license COPYING
%doc NEWS README.md
%{_datadir}/icons/hicolor/*/apps/org.gnome.Epiphany*
%{_datadir}/glib-2.0/schemas/org.gnome.epiphany.gschema.xml
%{_datadir}/glib-2.0/schemas/org.gnome.Epiphany.enums.xml
%{_bindir}/epiphany
%{_libexecdir}/epiphany/
%{_libdir}/epiphany/
%{_datadir}/epiphany
%{_mandir}/man*/*
%config(noreplace)%{_sysconfdir}/ld.so.conf.d/%{name}-%{_arch}.conf

%changelog
* Fri Sep 10 2021 lingsheng <lingsheng@huawei.com> - 3.38.5-2
- Delete rpath setting

* Thu Jun 24 2021 Wenlong Ding <wenlong.ding@turbolinux.com.cn> - 3.38.5-1
- Update to 3.38.5

* Thu Nov 5 2020 Liu wei bo <liuweibo10@huawei.com> - 3.36.4-1
- package init
